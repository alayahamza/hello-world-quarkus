package java.org.aios.hello.payload;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Hello {
    public String value;
}
