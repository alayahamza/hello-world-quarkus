package java.org.aios.hello.controller;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.org.aios.hello.payload.Hello;


public class HelloWorldController {

    @Path("hello")
    @Produces(MediaType.APPLICATION_JSON)
    public Hello hello() {
        return Hello.builder().value("Hello world !").build();
    }
}
