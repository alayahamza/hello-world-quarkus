FROM findepi/graalvm:19.2.1-native as graalBuild
COPY src /usr/src/app/src
COPY .mvn /usr/src/app/.mvn
COPY mvnw /usr/src/app
COPY pom.xml /usr/src/app
WORKDIR /usr/src/app
RUN GRAALVM_HOME=/graalvm ./mvnw package -DskipTests=true -Pnative

FROM alpine:3
EXPOSE 8080
COPY --from=graalBuild "/usr/src/app/target/hello-world-*-runner" /hello-world
RUN apk add libc6-compat
CMD /hello-world
